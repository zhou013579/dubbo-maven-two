package com.jk.service;

import com.jk.model.User;

import java.util.List;

public interface UserService {
    List<User> queryuser(User user);

    void adduser(User user);

    void deleteuserbyid(Integer id);

    User queryuserbyid(Integer id);

    void updateuser(User user);
}
