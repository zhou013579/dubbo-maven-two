package com.jk.controller;

import com.alibaba.dubbo.config.annotation.Reference;
import com.jk.model.User;
import com.jk.service.UserService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;

@Controller
@RequestMapping("user")
public class UserController {

    @Reference
    private UserService userService;

    /*
     *查询并跳转查询页面
     * */
    @RequestMapping("queryuser")
    public String queryuser(Model model, User user) {
        List<User> users=userService.queryuser(user);
        model.addAttribute("users", users);
        return "user/list";
    }
    /*
     *跳转增加页面
     * */
    @RequestMapping("toAdd")
    public String toAdd() {
        return "user/userAdd";
    }

    /*
     *增加并重定向
     * */
    @RequestMapping("adduser")
    public String adduser(User user) {
        userService.adduser(user);
        return "redirect:queryuser";
    }

    /*
     *删除并重定向
     * */
    @RequestMapping("deleteuserbyid")
    public String deleteuserbyid(Integer id) {
        userService.deleteuserbyid(id);
        return "redirect:queryuser";
    }

    /*
     *修改--根据id查询并跳转修改页面
     * */
    @RequestMapping("toEdit")
    public String toEdit(Model model,Integer id) {
        User user=userService.queryuserbyid(id);
        model.addAttribute("user", user);
        return "user/userEdit";
    }

    /*
     *修改并重定向
     * */
    @RequestMapping("updateuser")
    public String updateuser(User user) {
        userService.updateuser(user);
        return "redirect:queryuser";
    }
}
