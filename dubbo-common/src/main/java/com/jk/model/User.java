package com.jk.model;

import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;
import java.util.Date;

@Data
public class User implements Serializable {

    private Integer userid;

    private String  username;

    private String  usersex;

    private String  userage;

    private String  userphone;

    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date userdate;

    private Integer count;

}
