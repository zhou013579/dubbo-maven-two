package com.jk.service;

import com.alibaba.dubbo.config.annotation.Service;
import com.jk.mapper.UserMapper;
import com.jk.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

@Service
@Component
public class UserServiceImpl implements UserService{

    @Autowired
    private UserMapper userMapper;

    @Override
    public List<User> queryuser(User user) {
        return userMapper.queryuser(user);
    }

    @Override
    public void adduser(User user) {
        user.setCount(21);
        userMapper.adduser(user);
    }

    @Override
    public void deleteuserbyid(Integer id) {
        userMapper.deleteuserbyid(id);
    }

    @Override
    public User queryuserbyid(Integer id) {
        return userMapper.queryuserbyid(id);
    }

    @Override
    public void updateuser(User user) {
        user.setCount(321);
        userMapper.updateuser(user);
    }
}
