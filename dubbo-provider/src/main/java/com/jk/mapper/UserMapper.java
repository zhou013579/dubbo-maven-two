package com.jk.mapper;

import com.jk.model.User;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface UserMapper {
    List<User> queryuser(@Param("user") User user);

    void adduser(@Param("user") User user);

    void deleteuserbyid(@Param("id") Integer id);

    User queryuserbyid(@Param("id") Integer id);

    void updateuser(@Param("user") User user);
}
